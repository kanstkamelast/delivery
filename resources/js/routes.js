import Vue from 'vue';
import VueRouter from 'vue-router';

import Dashboard from '@/js/components/Dashboard';
import Login from '@/js/components/Auth/Login';
import Register from '@/js/components/Auth/Register';
import Users from '@/js/pages/SuperAdmin/Users';
import UserCreate from '@/js/pages/SuperAdmin/UserCreate';
import Clients from '@/js/pages/SuperAdmin/Clients';
import ClientCreate from '@/js/pages/SuperAdmin/ClientCreate';
import Countries from '@/js/pages/SuperAdmin/Countries';
import CountryCreate from '@/js/pages/SuperAdmin/CountryCreate';
import Shipment from '@/js/pages/SuperAdmin/Shipment';
import ShipmentCreate from '@/js/pages/SuperAdmin/ShipmentCreate';
import AddParcel from '@/js/pages/User/AddParcel'; 
import CurrentParcels from '@/js/pages/User/CurrentParcels';
import Customers from '@/js/pages/User/Customers';
import CreateCustomer from '@/js/pages/User/CreateCustomer';


Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                auth: true
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                auth: false
            }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: {
                auth: false
            }
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
            meta: {
                auth: true
            }
        },
        {
            path: '/users/create',
            name: 'userCreate',
            component: UserCreate,
            meta: {
                auth: true
            }
        },
        {
            path: '/users/:userId/edit',
            name: 'userEdit',
            component: UserCreate,
            meta: {
                auth: true
            }
        },
        {
            path: '/clients',
            name: 'clients',
            component: Clients,
            meta: {
                auth: true
            }
        },
        {
            path: '/clients/create',
            name: 'clientCreate',
            component: ClientCreate,
            meta: {
                auth: true
            }
        },
        {
            path: '/clients/:clientId/edit',
            name: 'clientEdit',
            component: ClientCreate,
            meta: {
                auth: true
            }
        },
        {
            path: '/settings',
            name: 'settings',
            component: ClientCreate,
            meta: {
                auth: true
            }
        },
        {
            path: '/countries',
            name: 'countries',
            component: Countries,
            meta: {
                auth: true
            }
        },
        {
            path: '/countries/create',
            name: 'countryCreate',
            component: CountryCreate,
            meta: {
                auth: true
            }
        },
        {
            path: '/countries/:countryId/edit',
            name: 'countryEdit',
            component: CountryCreate,
            meta: {
                auth: true
            }
        },
        {
            path: '/shipment',
            name: 'shipment',
            component: Shipment,
            meta: {
                auth: true
            }
        },
        {
            path: '/shipment/create',
            name: 'shipmentCreate',
            component: ShipmentCreate,
            meta: {
                auth: true
            }
        },
        {
            path: '/shipment/:shipmentId/edit',
            name: 'shipmentEdit',
            component: ShipmentCreate,
            meta: {
                auth: true
            }
        },

        {
            path: '/parcels/add',
            name: 'addParcel',
            component: AddParcel,
            meta: {
                auth: true
            }
        },
        {
            path: '/parcels/current',
            name: 'currentParcels',
            component: CurrentParcels,
            meta: {
                auth: true
            }
        },
        {
            path: '/parcels/sent',
            name: 'sentParcels',
            component: CurrentParcels,
            meta: {
                auth: true
            }
        },
        {
            path: '/parcels/sentByAdmin',
            name: 'sentByAdminParcels',
            component: CurrentParcels,
            meta: {
                auth: true
            }
        },
        {
            path: '/parcels/delivered',
            name: 'deliveredParcels',
            component: CurrentParcels,
            meta: {
                auth: true
            }
        },
        {
            path: '/parcels/received',
            name: 'receivedParcels',
            component: CurrentParcels,
            meta: {
                auth: true
            }
        },
        {
            path: '/parcels/rejected',
            name: 'rejectedParcels',
            component: CurrentParcels,
            meta: {
                auth: true
            }
        },

        /**
         * Customers
         */

         {
            path: '/customers',
            name: 'Customers',
            component: Customers,
            meta: {
                auth: true
            }
         },
         {
            path: '/customer/create',
            name: 'createCustomer',
            component: CreateCustomer,
            meta: {
                auth: true
            }
         },
         {
            path: '/customers/:customerId/edit',
            name: 'editCustomer',
            component: CreateCustomer,
            meta: {
                auth: true
            }
         },
    ]    
});


export default router;