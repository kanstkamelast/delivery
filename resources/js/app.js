import './bootstrap';
import Vue from 'vue';
import Vuetify from 'vuetify';
import Routes from '@/js/routes.js';
import App from '@/js/views/App';
import axios from 'axios';
import VueAxios from 'vue-axios';

import Menu from '@/js/layouts/Menu';

Vue.use(Vuetify);
Vue.use(Menu);
Vue.use(VueAxios, axios);

axios.defaults.baseURL = window.location.origin + '/api'; 

Vue.router = Routes;

Vue.use(require('@websanova/vue-auth'), {
    
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
 });

const app = new Vue({
    el: '#app',
    router: Routes,
    render: h => h(App),
});

export default app;