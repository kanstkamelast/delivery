export default {
  // constants
  // Parcel Types
  PT_REGULAR: 0,
  PT_LARGE: 1,
  // Regular Parcel Type values and coefs
  RPT_MIN: 25,
  RPT_MAX: 100,
  RPT_INSURANCE_FEE: 0.9,
  // Large Package Type values and coefs
  LPT_PER_M3: 210,
  LPT_PER_POUND: 1.5,
  LPT_FEE: 46,
  LPT_INSURANCE_FEE: 0.9,
  // inch to meter conversion
  IN2M: 0.0254
};
