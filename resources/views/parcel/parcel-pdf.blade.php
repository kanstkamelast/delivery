<?php
use App\Entities\Parcel; 
for ($report = 0; $report < 2; $report++): ?>
<table style="width:100%" class="customers">
    <tr>
        <th style="width: 40px;"><img src="{!! public_path('images/logo_sm.png') !!}" alt="logo" style="width: 40px;"></th>
      <th style="text-align:center" >Straight bill of lading - Custom declaration CN23</th>
    </tr>
</table>
<table style="width:100%" class="customers">
    <tr>
      <td style="width: 10%">{{$parcel->client->id_number}}-{{$parcel->id}}</td>
      <td style="width: 33%; text-align: center;">
          {!! DNS1D::getBarcodeHTML($parcel->client->id_number.'-'.$parcel->id, "C39") !!}
      </td>
      <td>
        Firm address: <br>
        {{$parcel->client->address}}, {{$parcel->client->state}}, {{$parcel->client->city}}, {{$parcel->client->country}}, Tel: {{$parcel->client->phone}}
      </td>
    </tr>
</table>
<table style="width:100%" class="customers">
    <tr>
      <td style="width: 39%;">
        <h4>SHIPPER:</h4>
        <p>{{$parcel->sender->first_name}} {{$parcel->sender->last_name}}</p>
        <p>{{$parcel->sender->company_name}}</p>
        <p>{{$parcel->sender->street}} {{$parcel->sender->building}} {{$parcel->sender->house_number}} {{$parcel->sender->zip_code}}, {{$parcel->sender->city}}, {{$parcel->sender->state}}, {{$parcel->sender->country}}</p>
        <p>{{$parcel->sender->phone_number}}</p>
        <p>{{$parcel->sender->email}}</p>
      </td>
      <td style="width: 39%;">
        <h4>RECEIVER:</h4>
        <p>{{$parcel->recipient->first_name}} {{$parcel->recipient->last_name}}</p>
        <p>{{$parcel->recipient->company_name}}</p>
        <p>{{$parcel->recipient->street}} {{$parcel->recipient->building}} {{$parcel->recipient->house_number}} {{$parcel->recipient->zip_code}}, {{$parcel->recipient->city}}, {{$parcel->recipient->state}}, {{$parcel->recipient->country}}</p>
        <p>{{$parcel->recipient->phone_number}}</p>
        <p>{{$parcel->recipient->email}}</p>
      </td>
      <td style="width: 22%;">
        <p>Date: {{$parcel->created_at}}</p>
        @if ($parcel->type === Parcel::PT_REGULAR)
          <p>Parcel Type: {{$parcel->shipment->type}}</p>
        @else
          <p>Parcel Type: Large Package</p>
        @endif
        @if ($parcel->type === Parcel::PT_REGULAR)
          <p>Delivery Time: {{$parcel->shipment->delivery_time}}</p>
        @else
          <p>Parcel Type: 6-8 weeks</p>
        @endif
        <p> Weight: {{$parcel->weight}} lbs. </p>
        @if (!$report)
          <p> <b>Price: ${{$parcel->total_fee}}</b> </p>
          @if ($parcel->insurance > 0)
            @if ($parcel->type === Parcel::PT_REGULAR)
              <p>(incl. insurance: ${{round($parcel->insurance * Parcel::RPT_INSURANCE_FEE, 2)}})</p>
            @elseif ($parcel->type === Parcel::PT_LARGE)
              <p>(incl. insurance: ${{round($parcel->insurance * Parcel::LPT_INSURANCE_FEE, 2)}})</p>
            @endif
          @else
            <p>(no insurance)</p>
          @endif
        @endif
      </td>
    </tr>
</table>
<table style="width:100%" class="customers">
  <tr>
    <td style="padding: auto; width: 30%; vertical-align: top;">
      <table style="border-collapse: collapse; width: 100%;">
        <tr>
          <th colspan="2">Contents</th>
        </tr>
        <tr>
            <th style="width: 75%">Description</th>
            <th style="width: 25%">Quantity</th>
        </tr>
        @foreach(json_decode($parcel->products) as $product)
        <tr>
            <td>{{$product->description}}</td>
            <td>{{$product->quantity}}</td>
        </tr>
        @endforeach
      </table>
    </td>
    <td style="width: 35%;">
      <table style="text-align: justify;">
        <tbody>
        <tr>
            <td  style=" width: 30%; font-size: 10px; padding: 0; border: 0;">
                ATTENTION!  Received  for  shipment  from  the  Shipper/Merchant  in apparent good order and condition unless otherwise stated herein, to be transported by any mode of transport for all or any part of the Carriage. This shipment is accepted for transport by  Newpost subject to all applicable Terms and Conditions and/or Tariff in effect on the date of acceptance of this shipment (and available upon request), to which Shipper/Merchant agrees to as if fully set forth and incorporated herein by accepting and  signing  this  Bill  of  Lading.  This  Bill  of  Lading  is nonnegotiable. Shipper/Merchant certifies that this shipment is in full compliance with all applicable laws or regulations, that all information furnished herein is accurate and complete, and that this shipment does not contain any money, specie, negotiable instruments, personal or bank checks, money orders, glass, fragile materials of any type, hazardous materials,  illegal  items,  explosives,  weapons,  ammunition,  toxins  or other  items  prohibited  for  transport  under  U.S.  law,  the  destination country or any country through or over which the shipment will be routed. Shipper/Merchant consents to inspection of this shipment.
            </td>
        </tr>
        <tr>
            <td style="font-size: 14px;">Shipper's Signature<br> <span style="font-size: 10px">By signing I accept all terms and conditions:</span>
                <br /><br /><br />
                <hr>
                <p style="text-align: center; font-size: 10px">(signature)</p>
            </td>
        </tr>
        </tbody>
      </table>
    </td>
    <td style="width: 35%;">
        <table style="text-align: justify;">
            <tbody>
                <tr>
                    <td style="width: 30%; font-size: 10px; padding: 0; border: 0;">
                        NOTICE OF LIMITATIONS OF LIABILITY:  Newpost may limit its liability for loss, damage or delay as permitted by law and as set forth in its Terms and Conditions and/or Tariff, which are available  upon  request.  Shipper/Merchant  may  declare  a  higher value and increase such liability, which will require payment of an increased freight charge. In no event will  Newpost have any liability for shipments that are improperly packed. Claims must  be  timely  made  in  writing  and  in  accordance  with  all applicable Terms and Conditions and/or Tariff then in effect.
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10px; ">CHECK CONTENTS BEFORE SIGNING <br> ATTENTION! In order to file a claim for missing items, a discrepancy report must be signed by the recipient as well as the delivery person. Without this document the claim will not be processed. Shipment is accepted without reservation except as set forth hereon on behalf of  Newpost
                        <br>
                        <br>
                        <br>
                        <hr>
                        <p style="text-align: center;">(full name)</p>
                        <br>
                        <br>
                        <br>
                        <hr>
                        <p style="text-align: center;">(date dd/mm/yyyy)</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
  </tr>
  <tr>
    <td colspan="3" style="text-align: center;">
        <b>DELIVERY TO RECIPIENT PAID FOR BY SHIPPER</b><br />
        <span style="font-size: 10px;">
            WE APPRECIATE YOUR BUSINESS. NewPost |
            68-01 Fresh Pond Rd Ridgewood, NY 11385, USA; Tel: +1(646) 243-14-30 |
            contact@newpost.us www.newpost.us
        </span>
    </td>
  </tr>
</table>
    <?php if ($report % 2 == 0) : ?>
        <p style="font-size: 6px;">&nbsp;</p>
    <?php endif; ?>
<?php endfor; ?>

<style>
  .customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
    margin: 0 auto;
  }

  .customers h4, .customers p {
    margin: 0;
  }
  
  .customers td, .customers th {
    border: 1px solid #ddd;
    padding: 8px;
    font-size: 15px;
  }
  
  .customers tr:nth-child(even){background-color: #f2f2f2;}
  
  .customers tr:hover {background-color: #ddd;}
  
  .customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #333333;
    color: white;
  }

  .customers table th {
      background: #fff;
      color: #333333;
  }

  hr{
    border: 0;
    border-bottom: 3px dashed #00000073;
    background: #fff;
  }

.page-break {
    page-break-after: always;
}
  </style>