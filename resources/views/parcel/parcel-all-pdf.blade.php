<?php
use App\Helpers\MoneyHelper;
use Carbon\Carbon;
?>

<table class="header">
    <tr class="bg-grey">
        <th class="logo"><img src="{!! public_path('images/logo_sm.png') !!}" alt="logo" /></th>
        <th class="our-requisites">
            NewPost 68-01 Fresh Pond<br />
            Rd Ridgewood, NY 11385,<br />
            contact@newpost.us
        </th>
        <th>
            Raport for {{$user->client->name}}<br />
            From: {{Carbon::createFromFormat('Y-m-d', $filteredData['dateFrom'])->format('m/d/Y')}} <br />
            To: {{Carbon::createFromFormat('Y-m-d', $filteredData['dateTo'])->format('m/d/Y')}} <br />
            Date: {{Carbon::now()->format('m/d/Y')}}
        </th>
        <th>
            Agent {{$user->client->name}}<br />
            Address: {{$user->client->address}} <br />
            Phone: {{$user->client->phone}}
        </th>
    </tr>
</table>
@php $total = 0 @endphp
@php $userDiscount = is_null($user->discount) ? 0 : $user->discount; @endphp
<table class="parcels">
    <tr>
        <th class="parcel-number">Parcel number</th>
        <th class="shipping-type">Shipping type</th>
        <th class="amount">Amount</th>
    </tr>
    @foreach($parcels as $parcel)
    <tr>
        <td class="parcel-number">{{$parcel->id}}</td>
        <td class="shipping-type">{{isset($parcel->shipment) ? $parcel->shipment->type : 'large'}}</td>
        @php $amount = (float) $parcel->total_fee @endphp
        <td class="amount">{{MoneyHelper::toMoney($amount)}}</td>
        @php $total = $total + $amount @endphp
    </tr>
    @endforeach
    <tr class="total">
        <td colspan="2" class="title">Total:</td>
        <td class="amount">{{MoneyHelper::toMoney($total)}}</td>
    </tr>
    <?php
    $afterDiscount = $total;
    if(isset($user->discount)) {
        $afterDiscount = round($total - ($total * (int) $user->discount / 100), 2) ;
    }
    ?>
    <tr class="discount">
        <td colspan="2" class="title">Payment for Newpost after discount {{$userDiscount}}%:</td>
        <td class="amount">{{MoneyHelper::toMoney($afterDiscount - $total)}}</td>
    </tr>
</table>
<table class="final-settlement">
    <tr>
        <td class="left">
            <h3>Final Settlement:</h3>
            - Payment for parcels for NEWPOST: {{MoneyHelper::toMoney($total)}}<br />
            - Discount for parcels: {{$userDiscount}}%<br />
            - Payment for NEWPOST after discount: {{MoneyHelper::toMoney($afterDiscount)}}<br />
            <h3>Balance: {{MoneyHelper::toMoney($user->money_limit - $afterDiscount)}}</h3>
        </td>
        <td>&nbsp;</td>
    </tr>
</table>

<style>
    table {
        width: 100%;
        border-collapse: collapse;
    }

    .header,
    .parcels,
    .final-settlement {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        width: 100%;
    }

    .header .bg-grey {
        color: white !important;
        background-color: #333333;
    }

    .header .logo,
    .header .logo img {
        width: 80px;
    }

    .parcels th {
        background: #ddd;
        text-align: center;
    }

    .parcels td.amount {
        text-align: right;
    }

    .parcels .total {
        font-weight: bold;
        background: #ddd;
    }

    .final-settlement h4,
    .final-settlement p {
        margin: 0;
    }

    .header th,
    .parcels th,
    .parcels td,
    .final-settlement td {
        border: 1px solid #ddd;
        padding: 8px;
        font-size: 15px;
    }

    .parcels tr:nth-child(even){
        background-color: #f2f2f2;
    }

    .parcels .discount {
        background: white;
    }

    .parcels .discount .title {
        text-align: right;
    }

    .final-settlement td {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        color: #333333;
    }

    .final-settlement .left {
        width: 50%;
    }

    hr {
        border: 0;
        border-bottom: 3px dashed #00000073;
        background: #fff;
    }

    .page-break {
        page-break-after: always;
    }
  </style>