<?php
use App\Entities\Shipment;
use App\Helpers\MoneyHelper;
use Carbon\Carbon;
use Picqer\Barcode\BarcodeGeneratorHTML;

$userUname = strtoupper(substr($user->client->name, 0, 3)) . str_pad($user->client->id, 3, '0', STR_PAD_LEFT)
    . '#' . str_pad($report->id_per_client, 4, '0', STR_PAD_LEFT);
$userFullId = $user->client->id_number . '-' . $user->id;
$barCodeGenerator = new BarcodeGeneratorHTML();
$barCode = $barCodeGenerator->getBarcode($userFullId, $barCodeGenerator::TYPE_CODE_128);

?>

<table class="header">
    <tr class="top-header">
        <th colspan="4">
            <h2>Pickup report</h2>
            From: {{Carbon::createFromFormat('Y-m-d', $filteredData['dateFrom'])->format('m/d/Y')}}&nbsp;&nbsp;&nbsp;
            To: {{Carbon::createFromFormat('Y-m-d', $filteredData['dateTo'])->format('m/d/Y')}}&nbsp;&nbsp;&nbsp;
            Date: {{Carbon::now()->format('m/d/Y')}}
        </th>
    </tr>
    <tr>
        <th class="logo bg-grey"><img src="{!! public_path('images/logo_sm.png') !!}" alt="logo" /></th>
        <th class="our-requisites">
            NewPost 68-01 Fresh Pond<br />
            Rd Ridgewood, NY 11385,<br />
            contact@newpost.us
        </th>
        <th class="bar-code">
            {!! $barCode !!}<br />
            {{ $userUname }}
        </th>
        <th>
            Agent {{$user->client->name}}<br />
            Address: {{$user->client->address}} <br />
            Phone: {{$user->client->phone}}
        </th>
    </tr>
</table>
@php 
$totalAmount = 0; $totalWeight = 0;
$shipmentTypes = array_fill_keys(Shipment::getTypes(), 0);
$shipmentTypes['large'] = 0;
$userDiscount = is_null($user->discount) ? 0 : $user->discount;
@endphp
<table class="parcels">
    <tr>
        <th class="parcel-number">Parcel number</th>
        <th class="weight">Weight</th>
        <th class="shipping-type">Shipping type</th>
        <th class="amount">Amount</th>
    </tr>
    @foreach($parcels as $parcel)
    <tr>
        @php $shipmentType = isset($parcel->shipment) ? $parcel->shipment->type : 'large'; @endphp
        <td class="parcel-number">{{$parcel->id}}</td>
        <td class="weight">{{(float) $parcel->weight}} lb</td>
        <td class="shipping-type">{{$shipmentType}}</td>
        @php $shipmentTypes[$shipmentType]++; @endphp
        @php $amount = (float) $parcel->total_fee @endphp
        <td class="amount">{{MoneyHelper::toMoney($amount)}}</td>
        @php $totalAmount = $totalAmount + $amount @endphp
        @php $totalWeight = $totalWeight + (float) $parcel->weight @endphp
    </tr>
    @endforeach
    <tr class="total">
        <td class="parcel-number">Total:</td>
        <td class="weight">{{$totalWeight}} lb</td>
        <td class="shipping-type">
            <table>
                <tr>
                    @foreach ($shipmentTypes as $type => $amount)
                    <td>{{$type}}: {{$amount}}</td>
                    @endforeach
                </tr>
            </table>
        </td>
        <td class="amount">{{MoneyHelper::toMoney($totalAmount)}}</td>
    </tr>
    <?php
    $afterDiscount = $totalAmount;
    if(isset($user->discount)) {
        $afterDiscount = round($totalAmount - ($totalAmount * (int) $user->discount / 100), 2) ;
    }
    ?>
    <tr class="discount">
        <td colspan="3" class="title">Payment for NEWPOST after discount {{$userDiscount}}%:</td>
        <td class="amount">{{MoneyHelper::toMoney($afterDiscount)}}</td>
    </tr>
</table>
<table class="final-settlement">
    <tr>
        <td class="left">
            <h3>Final Settlement:</h3>
            - Payment for parcels for NEWPOST: {{MoneyHelper::toMoney($totalAmount)}}<br />
            - Discount for parcels: {{$userDiscount}}%<br />
            - Payment for NEWPOST after discount: {{MoneyHelper::toMoney($afterDiscount)}}<br />
            <h3>To Pay: {{MoneyHelper::toMoney($afterDiscount)}}</h3>
        </td>
        <td rowspan="2" class="right">
            <table class="signature">
                <tr>
                    <td>Agent signature:</td>
                    <td>________________________</td>
                </tr>
                <tr>
                    <td>Date (mm/dd/yyyy):</td>
                    <td>________________________</td>
                </tr>
                <tr>
                    <td>Time:</td>
                    <td>________________________</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="final-agreements">
        <td>
            <table class="check">
                <tr>
                    <td>Check:</td>
                    <td>#___________$___________</td>
                </tr>
                <tr>
                    <td>Cash:</td>
                    <td>#_______________________</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<style>
    table {
        width: 100%;
        border-collapse: collapse;
    }

    .header,
    .parcels,
    .final-settlement {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        width: 100%;
    }

    .header .top-header {
        text-align: center;
    }

    .header .top-header  h2 {
        margin-top: 5px;
    }

    .header .bg-grey {
        color: white !important;
        background-color: #333333;
    }

    .header .logo,
    .header .logo img {
        width: 80px;
    }

    .header .bar-code {
        text-align: center;
    }

    .header .bar-code div {
        margin: 0 auto;
    }

    .parcels th {
        background: #ddd;
        text-align: center;
    }

    .parcels td.amount {
        text-align: right;
    }

    .parcels .total {
        font-weight: bold;
        background: #ddd;
    }

    .parcels .total .shipping-type {
        padding: 0;
    }

    .parcels .total .shipping-type td {
        text-align: center;
    }

    .final-settlement h4,
    .final-settlement p {
        margin: 0;
    }

    .header th,
    .parcels th,
    .parcels td,
    .final-settlement td {
        border: 1px solid #ddd;
        padding: 8px;
        font-size: 15px;
    }

    .parcels tr:nth-child(even){
        background-color: #f2f2f2;
    }

    .parcels .discount {
        background: white;
    }

    .parcels .discount .title {
        text-align: right;
    }

    .final-settlement td {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        color: #333333;
    }

    .final-settlement .left {
        width: 50%;
    }

    hr {
        border: 0;
        border-bottom: 3px dashed #00000073;
        background: #fff;
    }

    .final-settlement .right {
        padding: 0;
        vertical-align: top;
    }

    table.check,
    table.signature {
        font-weight: bold;
        vertical-align: top;
    }

    table.check td,
    table.signature td {
        border: none;
        padding: 15px 8px;
    }

    table.signature td {
        padding-top: 82px;
    }

    .page-break {
        page-break-after: always;
    }
</style>
