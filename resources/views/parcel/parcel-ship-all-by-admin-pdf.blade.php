<?php
use App\Entities\Shipment;
use App\Helpers\MoneyHelper;
use Carbon\Carbon;

$docHeader = 'Packing List #' . $report->user_id . "&ndash;" . $report->id_per_client;
?>
<body>
<script type="text/php">
    if (isset($pdf)) {
        $x = $pdf->get_width() - 105;
        $y = 35;
        $text = "{PAGE_NUM} of {PAGE_COUNT}";
        $font = $fontMetrics->get_font("Arial", "bold");
        $size = 12;
        $color = array(0,0,0);
        $pdf->page_text($x, $y, $text, $font, $size, $color);
    }
</script>
<div id="page-header">
    {!!$docHeader!!}: page
</div>
<table class="header">
    <tr class="bg-grey">
        <th class="logo"><img src="{!! public_path('images/logo_sm.png') !!}" alt="logo" /></th>
        <th>&nbsp;</th>
        <th class="our-requisites">
            NewPost 68-01 Fresh Pond<br />
            Rd Ridgewood, NY 11385,<br />
            Tel.: 646 243 1430, contact@newpost.us
        </th>
    </tr>
</table>

<h1>{!!$docHeader!!}</h1>

<table class="company-details">
    <tr>
        <th class="bg-blue-grey" colspan="2">COMPANY:</th>
    </tr>
    <tr>
        <td>NAME:</td>
        <td>NEW POST</td>
    </tr>
    <tr>
        <td>ADDRESS:</td>
        <td>68-01 Freshpond Road</td>
    </tr>
    <tr>
        <td>CITY / STATE / ZIP:</td>
        <td>Queens , NY, 11385</td>
    </tr>
    <tr>
        <td>TELEPHONE:</td>
        <td>646 243 1430</td>
    </tr>
</table>
<table class="report-details">
    <tr class="bg-blue-grey">
        <th>Order Date</th>
        <th>Purchase Order #</th>
        <th class="empty-cell">&nbsp;</th>
        <th>Packing Date</th>
    </tr>
    <tr>
        <td>{{Carbon::createFromFormat('Y-m-d H:i:s', $report->created_at)->format('m/d/Y')}}</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
</table>
@php $totalQuantity = 0; $totalWeight = 0;
$shipmentTypes = array_fill_keys(Shipment::getTypes(), 0);
$shipmentTypes['large'] = 0;
$userDiscount = is_null($user->discount) ? 0 : $user->discount;
@endphp
<table class="parcels">
    <tr class="bg-blue-grey">
        <th class="parcel-number">Parcel number #</th>
        <th class="description">Description</th>
        <th class="content">Content</th>
        <th class="quantity">Quantity</th>
        <th class="weight">Weight (lbs.)</th>
        <th class="sender">Sender Info</th>
        <th class="receiver">Receiver Info</th>
    </tr>
    @foreach ($parcels as $parcel)
    <tr>
        @php $shipmentType = isset($parcel->shipment) ? $parcel->shipment->type : 'large'; @endphp
        <td class="parcel-number">{{Auth::user()->id}}&ndash;{{$parcel->id}}</td>
        <td class="shipping-type">{{$shipmentType}}</td>
        @php $shipmentTypes[$shipmentType]++; @endphp
        @php $products = json_decode($parcel->products, true); @endphp
        <td class="content">
            <?php foreach ($products as $key => $value) {
                echo (isset($value['description']) ? $value['description'] : '') . '<br />';
            } ?>
        </td>
        <td class="quantity">
            <?php foreach($products as $key => $value) {
                if (isset($value['quantity'])) {
                    $totalQuantity += (int) $value['quantity'];
                    echo $value['quantity'];
                }
                echo '<br />';
            } ?></td>
        <td class="weight">{{(float) $parcel->weight}}</td>
        <td class="sender">
            @if (isset($parcel->sender))
            {{$parcel->sender->first_name}} {{$parcel->sender->last_name}} <br />
            Address: {{$parcel->sender->country}} {{$parcel->sender->state}} {{$parcel->sender->city}} {{$parcel->sender->zip_code}} {{$parcel->sender->street}} {{$parcel->sender->building}} {{$parcel->sender->house_number}}<br />
            Phone number: {{$parcel->sender->phone_number}}
            @endif
        </td>
        <td class="receiver">
            @if (isset($parcel->recipient))
            {{$parcel->recipient->first_name}} {{$parcel->recipient->last_name}} <br />
            Address: {{$parcel->recipient->country}} {{$parcel->recipient->state}} {{$parcel->recipient->city}} {{$parcel->recipient->zip_code}} {{$parcel->recipient->street}} {{$parcel->recipient->building}} {{$parcel->recipient->house_number}}<br />
            Phone number: {{$parcel->recipient->phone_number}}
            @endif
        </td>
        @php $totalWeight = $totalWeight + (float) $parcel->weight @endphp
    </tr>
    @endforeach
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr class="total">
        <td style="border: none">&nbsp;</td>
        <td style="border: none" colspan="2">Total Quantity and weight of Goods/Boxes</td>
        <td>{{$totalQuantity}}</td>
        <td>{{$totalWeight}}</td>
        <td style="border-bottom: none; border-right: none;">&nbsp;</td>
        <td style="border-bottom: none; border-left: none; border-right: none;">&nbsp;</td>
    </tr>
</table>
<form>
<table class="special-notes">
    <tr class="bg-blue-grey">
        <td colspan="2">Special Notes:</td>
    </tr>
    <tr>
        <td colspan="2" class="special-notes-cell">&nbsp;</td>
    </tr>
    <tr>
        <td class="signatures left">
            <p>Packed By:
                <span class="input-container"><input type="text" value="&nbsp;" /></span>
            </p>
        </td>
        <td class="signatures right">
            <p>Checked By:
                <span class="input-container"><input type="text" value="&nbsp;" /></span>
            </p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;Signature:
                <span class="input-container"><input type="text" value="&nbsp;" /></span>
            </p>
        </td>
    </tr>
</table>
</form>

<style>
    body {
        margin: 25px 0 0 0;
    }

    #page-header {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        height: 20px;
        text-align: right;
        padding-right: 100px;
    }
    table {
        width: 100%;
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
    }

    h1 {
        color: #004269;
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    }

    .bg-blue-grey {
        color: white !important;
        background-color: #004269;
    }

    .header,
    .parcels,
    .final-settlement {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        width: 100%;
    }

    .header,
    .header th,
    .header td {
        border: none;
        padding: 0;
    }

    .header .bg-grey {
        color: white !important;
        background-color: #333333;
    }

    .header .logo img {
        width: 80px;
        margin: 0;
        padding: 0;
    }

    .header .our-requisites {
        color: white;
        width: 350px;
    }

    .company-details {
        width: 25%;
    }

    .company-details th,
    .company-details td {
        border: 1px solid black;
        padding: 5px;
    }

    .report-details {
        width: 50%;
        margin-top: 30px;
        text-align: center;
    }

    .report-details th,
    .report-details td {
        border: 1px solid #ddd;
        padding: 5px;
    }

    .report-details .empty-cell {
        width: 25%;
    }

    .parcels {
        margin-top: 30px;
    }

    .parcels th {
        padding: 2px;
        text-align: center;
    }

    .parcels .total {
        font-weight: bold;
        background: white;
    }

    .parcels th,
    .parcels td,
    .final-settlement td {
        border: 1px solid #ddd;
        padding: 8px;
        font-size: 15px;
    }

    .parcels tr:nth-child(even){
        background-color: #f2f2f2;
    }

    .parcels .discount {
        background: white;
    }

    .parcels .discount .title {
        text-align: right;
    }

    .special-notes {
        margin-top: 30px;
        width: 630px;
        font-weight: bold;
    }

    .special-notes td {
        padding: 5px;
        border: 1px solid #ddd;
    }

    .special-notes .special-notes-cell {
        padding: 35px;
    }

    .special-notes td.signatures {
        border: none;
        vertical-align: top;
        padding: 10px 0;
    }

    .special-notes .signatures.right {
        width: 305px;
    }

    td.signatures p {
        vertical-align: middle;
    }
    td.signatures .input-container {
        width: 100%;
        display: inline;
        margin-top: 20px;
    }

    td.signatures input {
        border: 1px solid #ddd;
        display: inline-block;
    }
</style>
</body>