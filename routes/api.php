<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/register', 'Auth\AuthController@register');

Route::post('auth/login', 'Auth\AuthController@login');

Route::group(['middleware' => 'jwt.auth'], function() {
    Route::get('auth/user', 'Auth\AuthController@user');
    Route::post('auth/logout', 'Auth\AuthController@logout');
});

Route::group(['middleware' => 'jwt.refresh'], function() {
    Route::get('auth/refresh', 'Auth\AuthController@refresh');
});

Route::get('parcel/status', 'Api\Parcel\ParcelController@getParcelStatus');

Route::group(['middleware' => 'jwt.auth.token'], function() {

    /**
 * Users
 */

    Route::get('users/all', 'Api\User\UserController@allUsersForSuperAdmin');
    Route::get('users/{userId}', 'Api\User\UserController@getUserData');
    Route::post('users/create', 'Api\User\UserController@create');
    Route::post('users/update', 'Api\User\UserController@update');
    Route::post('users/delete', 'Api\User\UserController@delete');

    Route::get('clients/all', 'Api\User\ClientController@allClientsForSuperAdmin');
    Route::get('clients/{clientId}', 'Api\User\ClientController@getClientData');
    Route::post('clients/create', 'Api\User\ClientController@create');
    Route::post('clients/update', 'Api\User\ClientController@update');
    Route::post('clients/delete', 'Api\User\ClientController@delete');

    Route::get('countries/all', 'Api\Parcel\CountryController@allCountriesForSuperAdmin');
    Route::get('countries/{countryId}', 'Api\Parcel\CountryController@getCountryData');
    Route::post('countries/create', 'Api\Parcel\CountryController@create');
    Route::post('countries/update', 'Api\Parcel\CountryController@update');
    Route::post('countries/delete', 'Api\Parcel\CountryController@delete');

    Route::get('shipment/all', 'Api\Parcel\ShipmentController@allShipmentForSuperAdmin');
    Route::get('shipment/{shipmentId}', 'Api\Parcel\ShipmentController@getShipmentData');
    Route::post('shipment/create', 'Api\Parcel\ShipmentController@create');
    Route::post('shipment/update', 'Api\Parcel\ShipmentController@update');
    Route::post('shipment/delete', 'Api\Parcel\ShipmentController@delete');

    Route::get('customers/all', 'Api\Parcel\CustomerController@allCustomers');
    Route::get('customers/{customerId}', 'Api\Parcel\CustomerController@getCustomerData');
    Route::post('customers/create', 'Api\Parcel\CustomerController@create');
    Route::post('customers/update', 'Api\Parcel\CustomerController@update');
    Route::post('customers/delete', 'Api\Parcel\CustomerController@delete');

    Route::post('parcel/create', 'Api\Parcel\ParcelController@create');
    Route::post('parcel/change-status', 'Api\Parcel\ParcelController@changeStatus');
    Route::post('parcel/update-customers', 'Api\Parcel\ParcelController@updateCustomers');
    Route::get('parcel/all/current', 'Api\Parcel\ParcelController@allCurrent');
    Route::get('parcel/all/sent', 'Api\Parcel\ParcelController@allSent');
    Route::get('parcel/all/sentByAdmin', 'Api\Parcel\ParcelController@allSentByAdmin');
    Route::get('parcel/all/delivered', 'Api\Parcel\ParcelController@allDelivered');
    Route::get('parcel/all/received', 'Api\Parcel\ParcelController@allReceived');
    Route::get('parcel/all/rejected', 'Api\Parcel\ParcelController@allRejected');
    Route::get('parcel/all/reports', 'Api\Parcel\ParcelController@allSentReports');
    Route::get('parcel/all/adminReports', 'Api\Parcel\ParcelController@allAdminReports');
    Route::get('parcel/{parcelId}', 'Api\Parcel\ParcelController@getParcel');

    Route::post('print/parcel-all', 'Api\Parcel\ParcelController@printAll');    
    Route::post('print/release-all', 'Api\Parcel\ParcelController@releaseAll');    
    Route::post('print/ship-all-by-admin', 'Api\Parcel\ParcelController@releaseAll');
    Route::get('print/parcel/{parcelId}', 'Api\Parcel\ParcelController@printParcel');


    Route::get('dashboard/user', 'Api\DashboardController@getUserData');
});
