1.  composer install
2.  npm install
3.  create DB (utf8_general_ci)
4.  create .env file and change DB config inside
5.  php artisan migrate
6.  php artisan db:seed
7.  php artisan key:generate
8.  php artisan jwt:secret
8.  php artisan l5-swagger:generate
9.  php artisan serve

Then go to localhost:8000/
login - admin@admin.com
pass - adminadmin | documentation on /api/documentation

