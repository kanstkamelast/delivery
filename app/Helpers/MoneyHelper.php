<?php

namespace App\Helpers;


class MoneyHelper
{
    public static function toMoney($value, $symbol = '$', $decimals = 2)
    {
        $value = (float) $value;
        return ($value < 0 ? '- ' : '') . $symbol . number_format(abs($value), $decimals);
    }
}