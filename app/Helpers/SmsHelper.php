<?php

namespace App\Helpers;

use Twilio\Rest\Client;
use App\Entities\Customer;

class SmsHelper
{
    const MSG_PARCEL_ACCEPTED = "Thank you for using Newpost!\nNumber of your parcel {{id}}, {{shipment}}.\nYou can track your parcel status here newpost.us.\nThank you!";

    public static function sendParcelAccepted($sender, $parcel)
    {
        if ($sender instanceof Customer) {
            $sender = $sender->getAttributes();
        }

        $values = [];
        // customer attributes
        $allowedAttrs = [
            'last_name',
            'first_name',
            'company_name',
            'street',
            'building',
            'house_number',
            'zip_code',
            'city',
            'state',
            'country',
            'phone_number',
            'email'
        ];
        foreach ($allowedAttrs as $attribute) {
            $values[$attribute] = $sender[$attribute];
        }
        //parcel attributes
        $allowedAttrs = [
            'id',
            'shipment',
            'weight',
            'height',
            'length',
            'payment',
            'total_fee',
            'width',
        ];
        foreach ($allowedAttrs as $attribute) {
            $values[$attribute] = $attribute == 'shipment'
                ? (isset($parcel->shipment) ? $parcel->shipment->type : 'large') : $parcel->$attribute;
        }

        $personalizedMessage = self::fillPlaceholders($values, self::MSG_PARCEL_ACCEPTED);

        self::send(self::getPhoneNumber($sender), $personalizedMessage);
    }

    public static function send($number, $message)
    {
        $sid = env('TWILIO_SID');
        $token = env('TWILIO_TOKEN');
        $client = new Client($sid, $token);
        $options = [
            'from' => env('TWILIO_FROM'),
            'body' => $message,
        ];

        $client->messages->create($number, $options);

        return back()->with('success', 'Message sent!');
    }

    protected static function fillPlaceholders($values, $message)
    {
        foreach ($values as $attribute => $value) {
            $message = str_replace('{{' . $attribute . '}}', $value, $message);
        }

        return $message;
    }

    protected static function getPhoneNumber($sender)
    {
        if ($sender['phone_number'][0] == '1') {
            // add + to the begin of phone number, if not exists
            return '+' . $sender['phone_number'];
        }
        if ($sender['phone_number'][0] == '+') {
            // number has valid format
            return $sender['phone_number'];
        }
        if (is_numeric($sender['phone_number'][0])) {
            // add +1 code if number starts with any other digit
            return '+1' . $sender['phone_number'];
        }
        return false;
    }
}