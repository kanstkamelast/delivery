<?php

namespace App;


use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Entities\Status;
use Auth;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'status', 'client_id', 'password', 'discount', 'money_limit'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function status()
    {
        return app()->make(Status::class)->get($this->status);
    }

    public function client()
    {
        return $this->hasOne('App\Entities\Client', 'id', 'client_id');
    }

    public function getByRole()
    {
        if(Auth::user()->status() == 'Super Admin')
            return $this->with('client')->get();
        if(Auth::user()->status() == 'Client Admin')
            return $this->where('status', '!=', 0)->with('client')->get();
        if(Auth::user()->status() == 'User')
            return null;
    }
}
