<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Client;
use App\Http\Requests\ClientCreateFormRequest;

class ClientController extends Controller
{
    protected $client;

    public function __construct(Client $client)
    {
        //parent::__construct();
        $this->client = $client;
    }

    /**
     * @OA\Get(
     *     path="/clients/all",
     *     tags={"clients"},
     *     summary="all clients For Super Admin",
     *     operationId="allClientsForSuperAdmin",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),  
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function allClientsForSuperAdmin()
    {
        return $this->client->all();
    }
/**
     * @OA\Post(
     *     path="/clients/create",
     *     tags={"clients"},
     *     summary="Create client",
     *     operationId="create",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="address",
     *         in="query",
     *         description="address",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="country name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="state",
     *         in="query",
     *         description="state",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="city",
     *         in="query",
     *         description="city",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="country",
     *         in="query",
     *         description="country",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         description="phone",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="id_number",
     *         in="query",
     *         description="id_number",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="user_percent",
     *         in="query",
     *         description="user_percent",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="fee",
     *         in="query",
     *         description="fee",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function create(ClientCreateFormRequest $request)
    {
        $client = new Client($request->client);
        $client->save();
        return response([
            'status' => 'success',
        ], 200);
    }
/**
     * @OA\Post(
     *     path="/clients/update",
     *     tags={"clients"},
     *     summary="update client",
     *     operationId="update",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="address",
     *         in="query",
     *         description="address",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="country name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="state",
     *         in="query",
     *         description="state",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="city",
     *         in="query",
     *         description="city",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="country",
     *         in="query",
     *         description="country",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         description="phone",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="id_number",
     *         in="query",
     *         description="id_number",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="user_percent",
     *         in="query",
     *         description="user_percent",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="fee",
     *         in="query",
     *         description="fee",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function update(Request $request)
    {
        Client::find($request->client['id'])->update($request->client);
        return response([
            'status' => 'success',
        ], 200);
    }
/**
     * @OA\Post(
     *     path="/clients/delete",
     *     tags={"clients"},
     *     summary="delete client",
     *     operationId="delete",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="client id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function delete(Request $request)
    {
        Client::find($request->client['id'])->delete();
        return response([
            'status' => 'success',
        ], 200);
    }
/**
     * @OA\Post(
     *     path="/clients/{clientId}",
     *     tags={"clients"},
     *     summary="get client data",
     *     operationId="getClientData",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="clientId",
     *         in="path",
     *         description="client id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function getClientData($clientId)
    {
        $client = Client::find((int) $clientId);
        if(!is_null($client))
            return response(['status' => 'success', 'client' => $client]);
        return response(['status' => 'error']);    
    }
}
