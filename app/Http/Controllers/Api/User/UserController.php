<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserFormRequest;
use App\Http\Requests\UpdateUserFormRequest;
use App\User;
use App\Entities\Status;
use Hash;
use Mail;
use Auth;

class UserController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        //parent::__construct();
        $this->user = $user;
    }
    /**
     * @OA\Get(
     *     path="/users/all",
     *     tags={"users"},
     *     summary="all users For Super Admin",
     *     operationId="allUsersForSuperAdmin",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),  
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function allUsersForSuperAdmin()
    {
        return $this->user->getByRole();
    }
    /**
     * @OA\Post(
     *     path="/users/create",
     *     tags={"users"},
     *     summary="Create user",
     *     operationId="create",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="email",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="status",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="client_id",
     *         in="query",
     *         description="client_id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="password",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="discount",
     *         in="query",
     *         description="discount",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="money_limit",
     *         in="query",
     *         description="money_limit",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function create(CreateUserFormRequest $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->user['password']);
        $user->client_id = $request->client_id;
        $user->status = app()->make(Status::class)->getIdByName($request->status);
        $user->save();

        return response([
            'status' => 'success',
        ], 200);
    }
    /**
     * @OA\Post(
     *     path="/users/update",
     *     tags={"users"},
     *     summary="update user",
     *     operationId="update",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="email",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="status",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="client_id",
     *         in="query",
     *         description="client_id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="password",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="discount",
     *         in="query",
     *         description="discount",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="money_limit",
     *         in="query",
     *         description="money_limit",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function update(UpdateUserFormRequest $request)
    {
        $updateData = $request->user;
        $updateData['status'] = app()->make(Status::class)->getIdByName($updateData['status']);
        if(is_null($request->user['password'])) unset($updateData['password']); else $updateData['password'] = bcrypt($updateData['password']);
        
        User::find($request->user['id'])->update($updateData);
        return response([
            'status' => 'success',
        ], 200);
    }
    /**
     * @OA\Post(
     *     path="/users/delete",
     *     tags={"users"},
     *     summary="delete user",
     *     operationId="delete",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="user id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function delete(Request $request)
    {
        User::find($request->user['id'])->delete();
        return response([
            'status' => 'success',
        ], 200);
    }
    /**
     * @OA\Get(
     *     path="/users/{userId}",
     *     tags={"users"},
     *     summary="get user data",
     *     operationId="getUserData",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="userId",
     *         in="query",
     *         description="user id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function getUserData($userId)
    {
        $user = User::find((int) $userId);
        if(!is_null($user))
            return response(['status' => 'success', 'user' => $user, 'user_role' => $user->status()]);
        return response(['status' => 'error']);    
    }
}
