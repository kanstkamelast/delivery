<?php

namespace App\Http\Controllers\Api\Parcel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Shipment;
use App\Http\Requests\ShipmentCreateFormRequest;
use App\Http\Requests\ShipmentUpdateFormRequest;

class ShipmentController extends Controller
{
    protected $shipment;

    public function __construct(Shipment $shipment)
    {
        //parent::__construct();
        $this->shipment = $shipment;
    }

    /**
     * @OA\Get(
     *     path="/shipment/all",
     *     tags={"shipment"},
     *     summary="all shipment For Super Admin",
     *     operationId="allShipmentForSuperAdmin",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),  
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function allShipmentForSuperAdmin()
    {
        return $this->shipment->all();
    }
    /**
     * @OA\Post(
     *     path="/shipment/create",
     *     tags={"shipment"},
     *     summary="Create shipment",
     *     operationId="create",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="type",
     *         in="query",
     *         description="type",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="country name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="payment",
     *         in="query",
     *         description="payment",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="comments",
     *         in="query",
     *         description="comments",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="delivery_time",
     *         in="query",
     *         description="delivery time",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function create(ShipmentCreateFormRequest $request)
    {
        Shipment::create($request->all());
     
        return response([
            'status' => 'success',
        ], 200);
    }
    /**
     * @OA\Post(
     *     path="/shipment/update",
     *     tags={"shipment"},
     *     summary="update shipment",
     *     operationId="update",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="type",
     *         in="query",
     *         description="type",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="country name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="payment",
     *         in="query",
     *         description="payment",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="comments",
     *         in="query",
     *         description="comments",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="delivery_time",
     *         in="query",
     *         description="delivery time",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function update(ShipmentUpdateFormRequest $request)
    {
        Shipment::find($request->shipmentId)->update($request->all());
        return response([
            'status' => 'success',
        ], 200);
    }
    /**
     * @OA\Post(
     *     path="/shipment/delete",
     *     tags={"shipment"},
     *     summary="delete shipment",
     *     operationId="delete",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="shipment id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function delete(Request $request)
    {
        Shipment::find($request->shipment['id'])->delete();
        return response([
            'status' => 'success',
        ], 200);
    }
    /**
     * @OA\Get(
     *     path="/shipment/{shipmentId}",
     *     tags={"shipment"},
     *     summary="Get shipment",
     *     operationId="getShipmentData",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="shipmentId",
     *         in="query",
     *         description="shipment id",
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),  
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function getShipmentData($shipmentId)
    {
        $shipment = Shipment::find((int) $shipmentId);
        if(!is_null($shipment))
            return response(['status' => 'success', 'shipment' => $shipment]);
        return response(['status' => 'error']);    
    }
}
