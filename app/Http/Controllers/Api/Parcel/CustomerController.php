<?php

namespace App\Http\Controllers\Api\Parcel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Customer;
use App\Http\Requests\CustomerCreateFormRequest;

class CustomerController extends Controller
{
    protected $customer;

    public function __construct(Customer $customer)
    {
        //parent::__construct();
        $this->customer = $customer;
    }

        /**
     * @OA\Get(
     *     path="/customers/all",
     *     tags={"customers"},
     *     summary="Get all customers",
     *     operationId="allCustomers",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="all customers",
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function allCustomers()
    {
        $customers = $this->customer->all();
        $customers->map(function($customer){
            $customer->full_name = $customer->first_name.' '.$customer->last_name;
        });
        
        return $customers;
    }

    /**
     * @OA\Post(
     *     path="/customers/create",
     *     tags={"customers"},
     *     summary="Create customer",
     *     operationId="create",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="last_name",
     *         in="query",
     *         description="last name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="first_name",
     *         in="query",
     *         description="first name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="company_name",
     *         in="query",
     *         description="company name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="street",
     *         in="query",
     *         description="street",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="building",
     *         in="query",
     *         description="building",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="house_number",
     *         in="query",
     *         description="house number",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="zip_code",
     *         in="query",
     *         description="zip code",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="city",
     *         in="query",
     *         description="city",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="state",
     *         in="query",
     *         description="state",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="country",
     *         in="query",
     *         description="country",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="phone_number",
     *         in="query",
     *         description="phone number",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="email",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function create(Request $request)
    {
        Customer::create($request->customer);
        
        return response([
            'status' => 'success',
        ], 200);
    }
    /**
     * @OA\Put(
     *     path="/customers/update",
     *     tags={"customers"},
     *     summary="Update customer",
     *     operationId="update",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="last_name",
     *         in="query",
     *         description="last name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="first_name",
     *         in="query",
     *         description="first name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="company_name",
     *         in="query",
     *         description="company name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="street",
     *         in="query",
     *         description="street",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="building",
     *         in="query",
     *         description="building",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="house_number",
     *         in="query",
     *         description="house number",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="zip_code",
     *         in="query",
     *         description="zip code",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="city",
     *         in="query",
     *         description="city",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="state",
     *         in="query",
     *         description="state",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="country",
     *         in="query",
     *         description="country",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="phone_number",
     *         in="query",
     *         description="phone number",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="email",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function update(CustomerCreateFormRequest $request)
    {
        Customer::find($request->customer['id'])->update($request->customer);
        return response([
            'status' => 'success',
        ], 200);
    }
    /**
     * @OA\Delete(
     *     path="/customers/delete",
     *     tags={"customers"},
     *     summary="Delete customer",
     *     operationId="delete",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="Customer id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function delete(Request $request)
    {
        Customer::find($request->customer['id'])->delete();
        return response([
            'status' => 'success',
        ], 200);
    }
    /**
     * @OA\Get(
     *     path="/customers/{customerId}",
     *     tags={"customers"},
     *     summary="Delete customer",
     *     operationId="getCustomerData",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="customerId",
     *         in="path",
     *         description="Customer id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function getCustomerData($customerId)
    {
        $customer = Customer::find((int) $customerId);
        if(!is_null($customer))
            return response(['status' => 'success', 'customer' => $customer]);
        return response(['status' => 'error']);    
    }
}
