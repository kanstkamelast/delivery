<?php

namespace App\Http\Controllers\Api\Parcel;

use App\Entities\ParcelsRelease;
use App\Helpers\SmsHelper;
use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\Concerns;
use App\Http\Controllers\Controller;
use App\Entities\Customer;
use App\Entities\Parcel;
use PDF;
use DB;
use View;
use Auth;
use Exception;
use Carbon\Carbon;

class ParcelController extends Controller
{
    protected $parcel;

    public function __construct(Parcel $parcel)
    {
        $this->parcel = $parcel;
    }

       /**
     * @OA\Post(
     *     path="/parcel/create",
     *     tags={"parcel"},
     *     summary="Create parcel",
     *     operationId="create",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="country_id",
     *         in="query",
     *         description="country id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="shipment_id",
     *         in="query",
     *         description="shipment id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="sender_id",
     *         in="query",
     *         description="sender id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="client_id",
     *         in="query",
     *         description="client id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="user_id",
     *         in="query",
     *         description="user id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="recipient_id",
     *         in="query",
     *         description="recipient id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="weight",
     *         in="query",
     *         description="weight",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="height",
     *         in="query",
     *         description="height",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="length",
     *         in="query",
     *         description="length",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="payment",
     *         in="query",
     *         description="payment",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="width",
     *         in="query",
     *         description="width",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="products",
     *         in="query",
     *         description="products",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="status_name",
     *         in="query",
     *         description="status name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="status_comment",
     *         in="query",
     *         description="status comment",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function create(Request $request)
    {
        if (Auth::user()->status() == "Super Admin") {
            return response([
                'status' => 'error',
                'msg' => 'Access forbidden'
            ], 403);
        }
        $sender = $request->sender;
        $recipient = $request->recipient;
        
        if(array_key_exists('id', $sender)) {
            $senderId = $sender['id'];
        } else {
            $customer = Customer::where('first_name', $sender['first_name'])
                ->where('last_name', $sender['last_name'])
                ->first();
            if ($customer) {
                return response([
                    'status' => 'error',
                    'msg' => 'Name must be unique. ' . $sender['first_name']
                        . ' ' . $sender['last_name'] . ' already taken. Choose customer from the list.'
                ], 200);
            }
            $senderId = Customer::create($request->sender)->id;
        }

        if(array_key_exists('id', $recipient)) {
            $recipientId = $recipient['id'];
        } else {
            $customer = Customer::where('first_name', $recipient['first_name'])
                ->where('last_name', $recipient['last_name'])
                ->first();
            if ($customer) {
                return response([
                    'status' => 'error',
                    'msg' => 'Name must be unique. ' . $recipient['first_name']
                        . ' ' . $recipient['last_name'] . ' already taken. Choose customer from the list'
                ], 200);
            }
            $recipientId = Customer::create($request->recipient)->id;
        }

        $parcel = new Parcel;
        $parcel->sender_id = $senderId;
        $parcel->recipient_id = $recipientId;
        $parcel->country_id = $request->parcel['country_id'];
        if (($parcel->type = $request->parcel['type']) === Parcel::PT_REGULAR) {
            $parcel->shipment_id = $request->parcel['shipment_id'];
        }
        $parcel->user_id = Auth::user()->id;
        $parcel->client_id = Auth::user()->client_id;
        $parcel->weight = $request->parcel['weight'];
        $parcel->height = $request->parcel['height'];
        $parcel->length = $request->parcel['length'];
        $parcel->payment = $request->parcel['payment'];
        $parcel->width = $request->parcel['width'];
        $parcel->insurance = $request->parcel['insurance'];
        $parcel->products = json_encode($request->parcel['products']);
        $parcel->total_fee = round((float) $request->parcel['total_fee'], 2);

        if ($parcel->save()) {
            try {
                SmsHelper::sendParcelAccepted($sender, $parcel);
            } catch (Exception $e) {
                report($e);
            }

            return response([
                'status' => 'success',
                'parcel_id' => $parcel->id,
            ], 200);
        }
        return response([
            'status' => 'error',
            'msg' => 'Parcel was not saved'
        ], 500);
    }
       /**
     * @OA\Get(
     *     path="/parcel/all/current",
     *     tags={"parcel"},
     *     summary="all current parcel",
     *     operationId="allCurrent",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function allCurrent(Request $request)
    {
        return $this->parcel->allByStatus($request->all(), null);
    }
       /**
     * @OA\Get(
     *     path="/parcel/all/sent",
     *     tags={"parcel"},
     *     summary="all sent parcel",
     *     operationId="allSent",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function allSent(Request $request)
    {
        return $this->parcel->allByStatus($request->all(), 'Shipped');
    }

    public function allSentByAdmin(Request $request)
    {
        if (Auth::user()->status() != "Super Admin") {
            return response([
                'status' => 'error',
                'msg' => 'Access forbidden'
            ], 403);
        }
        return $this->parcel->allByStatus($request->all(), 'ShippedByAdmin');
    }

    public function allDelivered(Request $request)
    {
        if (Auth::user()->status() != "Super Admin") {
            return response([
                'status' => 'error',
                'msg' => 'Access forbidden'
            ], 403);
        }
        return $this->parcel->allByStatus($request->all(), 'Delivered');
    }
       /**
     * @OA\Get(
     *     path="/parcel/all/received",
     *     tags={"parcel"},
     *     summary="all received parcel",
     *     operationId="allReceived",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function allReceived(Request $request)
    {
        return $this->parcel->allByStatus($request->all(), 'Received');
    }
       /**
     * @OA\Get(
     *     path="/parcel/all/rejected",
     *     tags={"parcel"},
     *     summary="all rejected parcel",
     *     operationId="allRejected",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function allRejected(Request $request)
    {
        return $this->parcel->allByStatus($request->all(), 'Rejected');
    }

    public function allSentReports(Request $request)
    {
        return ParcelsRelease::allReports($request->all());
    }

    public function allAdminReports(Request $request)
    {
        if (Auth::user()->status() != "Super Admin") {
            return response([
                'status' => 'error',
                'msg' => 'Access forbidden'
            ], 403);
        }
        return ParcelsRelease::allAdminReports($request->all());
    }
       /**
     * @OA\Get(
     *     path="/parcel/{parcelId}",
     *     tags={"parcel"},
     *     summary="get parcel",
     *     operationId="getParcel",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="parcelId",
     *         in="header",
     *         description="Parcel id",
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function getParcel($parcelId)
    {
        return Parcel::with('recipient', 'sender', 'shipment', 'country')->find($parcelId);
    }

    public function getParcelStatus(Request $request)
    {
        if (empty($request->parcelId)) {
            return response([
                'status' => 'error',
                'msg' => 'Bad request',
            ], 400);
        }
        if (empty($parcel = Parcel::find($request->parcelId))) {
            return response([
                'status' => 'success',
                'parcel_status' => 'Parcel not found',
            ], 200);
        }
        $status = empty($parcel->status_name) ? 'Shipped' : $parcel->status_name;
        $statusesForUser = [
            'Shipped' => 'In process',
            'ShippedByAdmin' => 'Shipped',
            'Delivered' => 'Out for delivery',
            'Received' => 'Delivered',
        ];
        return response([
            'status' => 'success',
            'parcel_status' => empty($statusesForUser[$status])
                ? 'unknown status' : $statusesForUser[$status],
        ], 200);
    }

    /**
     * @OA\Get(
     *     path="/print/parcel/{parcelId}",
     *     tags={"print"},
     *     summary="Print parcel by id",
     *     operationId="print",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="parcelId",
     *         in="path",
     *         description="parcel id",
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    
    public function printParcel($parcelId)
    {
        $parcel = Parcel::with('recipient', 'sender', 'shipment', 'country', 'client')->find($parcelId);
        $data = compact('parcel');
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('parcel.parcel-pdf', $data)->setPaper('a3', 'portrait');

        return $pdf->stream();
    }

    /**
     * @OA\Post(
     *     path="/print/parcel-all",
     *     tags={"print"},
     *     summary="Print all parcel",
     *     operationId="printAll",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function printAll(Request $request)
    {
        $filteredData = $request->only('reportId');
        $parcels = $this->parcel->allByStatus($request->all(), $request->status);
        $report = isset($filteredData['reportId']) ? ParcelsRelease::find($filteredData['reportId']) : null;

        $filteredData = $this->processDates($request->only('reportId'), $parcels, $report);

        $user = empty($report) ? Auth::user() : $report->user;
        $data = compact('parcels', 'filteredData', 'user', 'report');

        $pdf = PDF::loadView('parcel.parcel-all-pdf', $data)->setPaper('a3', 'portrait');
        return $pdf->stream();
    }

    /**
     * @OA\Post(
     *     path="/print/release-all",
     *     tags={"print"},
     *     summary="Print all parcel",
     *     operationId="releaseAll",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function releaseAll(Request $request)
    {
        ini_set('memory_limit', '2048M');
        // only admin allowed to send shipped parcels further
        $isAdminReport = empty($request->reportId) && $request->status == "Shipped"
            || !empty($request->reportId) && $request->status == "ShippedByAdmin";
        if ($isAdminReport && Auth::user()->status() != "Super Admin") {
            return response([
                'status' => 'error',
                'msg' => 'Access forbidden'
            ], 403);
        }
        $filteredData = $request->only('reportId');
        $parcels = $this->parcel->allByStatus($request->only('reportId'), $request->status);
        $id = false;
        if (!empty($filteredData['reportId'])) {
            $id = $filteredData['reportId'];
        } elseif (empty($request->status)) {
            // user sends parcels to admin
            $id = ParcelsRelease::send($parcels);
        } elseif ($request->status == "Shipped") {
            // admin sends parcels container further
            $id = ParcelsRelease::send($parcels, "Shipped");
        } else {
            return response([
                'status' => 'error',
            ], 500);
        }
        if ($id === false) {
            return response([
                'status' => 'error',
                'msg' => 'Bad request',
            ], 400);
        }
        $report = ParcelsRelease::find($id);
        $user = $report->user;
        $view = $isAdminReport ? 'parcel.parcel-ship-all-by-admin-pdf' : 'parcel.parcel-release-pdf';
        $filteredData = $this->processDates($request->only('reportId'), $parcels, $report);

        $data = compact('parcels', 'filteredData', 'user', 'report');
        $pdf = PDF::loadView($view, $data);
        $pdf->getDomPDF()->getOptions()->setIsPhpEnabled(true);
        $pdf->setPaper('a3', $isAdminReport ? 'landscape' : 'portrait');

        return $pdf->stream();
    }

    /**
     * @OA\Post(
     *     path="/parcel/change-status",
     *     tags={"parcel"},
     *     summary="Change parcel status",
     *     operationId="changeStatus",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="parcel_id",
     *         in="query",
     *         description="parcel id",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="status name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="comment",
     *         in="query",
     *         description="status comment",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function changeStatus(Request $request)
    {
        $parcel = Parcel::find($request->parcel['id']);
        if ($parcel) {
            $parcel->status_name = $request->status['name'];
            $parcel->status_comment = $request->status['comment'];
            $parcel->save();

            return response([
                'status' => 'success',
            ], 200);
        }
        return null;
    }

    public function updateCustomers(Request $request)
    {
        $senderData = $request->all()['sender'];
        $recipientData = $request->all()['recipient'];

        $sender = Customer::find($senderData['id']);
        $recipient = Customer::find($recipientData['id']);

        if (!$sender->update($senderData)) {
            return response([
                'status' => 'error',
                'msg' => 'Could not save Sender.'
            ], 200);
        }
        if (!$recipient->update($recipientData)) {
            return response([
                'status' => 'error',
                'msg' => 'Could not save Recipient.'
            ], 200);
        }
        return response([
            'status' => 'success',
        ], 200);
    }

    protected function processDates($data, $parcels, $report = null)
    {
        // if this is "print all parcels" report
        if (empty($report)) {
            $data['dateFrom'] = $parcels->isEmpty()
                ? Carbon::today()->format('Y-m-d')
                : Carbon::createFromFormat('Y-m-d H:i:s', $parcels[0]->created_at)->format('Y-m-d');
            $data['dateTo'] = Carbon::today()->format('Y-m-d');

            return $data;
        }
        // if this is "send all parcels" report
        $prevReport = ParcelsRelease::where('user_id', $report->user_id)
            ->where('id', '<', $report->id)
            ->orderBy('created_at', 'desc')
            ->first();
        // if this is the very first report of the user
        if (empty($prevReport)) {
            $data['dateFrom'] = $parcels->isEmpty()
                ? Carbon::today()->format('Y-m-d')
                : Carbon::createFromFormat('Y-m-d H:i:s', $parcels[0]->created_at)->format('Y-m-d');
        } else {
            $data['dateFrom'] = Carbon::createFromFormat('Y-m-d H:i:s', $prevReport->created_at)
                ->format('Y-m-d');
        }
        $data['dateTo'] = Carbon::createFromFormat('Y-m-d H:i:s', $report->created_at)
            ->format('Y-m-d');

        return $data;
    }
}
