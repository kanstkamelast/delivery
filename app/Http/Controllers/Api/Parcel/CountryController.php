<?php

namespace App\Http\Controllers\Api\Parcel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Country;
use App\Http\Requests\CountryCreateFormRequest;

class CountryController extends Controller
{
    protected $country;

    public function __construct(Country $country)
    {
        //parent::__construct();
        $this->country = $country;
    }

    /**
     * @OA\Get(
     *     path="/countries/all",
     *     tags={"countries"},
     *     summary="Get all countries for Super Admin",
     *     operationId="allCountriesForSuperAdmin",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="all countries",
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function allCountriesForSuperAdmin()
    {
        return $this->country->all();
    }

    /**
     * @OA\Post(
     *     path="/countries/create",
     *     tags={"countries"},
     *     summary="Create country",
     *     operationId="create",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="country name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function create(CountryCreateFormRequest $request)
    {
        $country = new Country;
        $country->name = $request->name;
        $country->save();
        return response([
            'status' => 'success',
        ], 200);
    }

    /**
     * @OA\Put(
     *     path="/countries/update",
     *     tags={"countries"},
     *     summary="Update country",
     *     operationId="update",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="country id",
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="country name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function update(CountryCreateFormRequest $request)
    {
        Country::find($request->country['id'])->update($request->country);
        return response([
            'status' => 'success',
        ], 200);
    }

        /**
     * @OA\Delete(
     *     path="/countries/delete",
     *     tags={"countries"},
     *     summary="Delete country",
     *     operationId="delete",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="country id",
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),  
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function delete(Request $request)
    {
        Country::find($request->country['id'])->delete();
        return response([
            'status' => 'success',
        ], 200);
    }
    
    /**
     * @OA\Get(
     *     path="/countries/{countryId}",
     *     tags={"countries"},
     *     summary="Get country",
     *     operationId="getCountryData",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Parameter(
     *         name="countryId",
     *         in="query",
     *         description="country id",
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),  
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function getCountryData($countryId)
    {
        $country = Country::find((int) $countryId);
        if(!is_null($country))
            return response(['status' => 'success', 'country' => $country]);
        return response(['status' => 'error']);    
    }
}
