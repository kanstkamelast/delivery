<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Parcel;
use App\Entities\Customer;

class DashboardController extends Controller
{

    protected $parcel;
    protected $customer;

    public function __construct(Parcel $parcel, Customer $customer)
    {
        $this->parcel = $parcel;
        $this->customer = $customer;
    }
    /**
     * @OA\Get(
     *     path="/dashboard/user",
     *     tags={"dashboard"},
     *     summary="get user data for dashboard",
     *     operationId="getUserData",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="status success"
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */
    public function getUserData()
    {
        return response()
        ->json([
            'currentParcels' => $this->parcel->currentParcels(),
            'sentParcels' => $this->parcel->sentParcelsToday(),
            'receivedParcels' => $this->parcel->receivedParcelsToday(),
            'allCustomers' => $this->customer->allCustomers(),
            'monthGrouped' => $this->parcel->monthGrouped(),
            'countryGrouped' => $this->parcel->countryGrouped(),
        ]);
    }

}
