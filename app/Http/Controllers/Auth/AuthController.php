<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterFormRequest;
use App\User;
use Auth;

class AuthController extends Controller
{

    /**
     * @OA\Post(
     *     path="/auth/register",
     *     tags={"auth"},
     *     summary="Register user",
     *     operationId="register",
     * @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="Email for authorization",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     * @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="Password for authorization",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     * @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="Name",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     * @OA\Parameter(
     *         name="userAgree",
     *         in="query",
     *         description="User agreement",
     *         @OA\Schema(
     *             type="boolean",
     *         )
     *     ),
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),    
     * @OA\Response(
     *         response=200,
     *      description="Registered"
     *     )
     * )
     */

    public function register(RegisterFormRequest $request)
    {
        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->save();
        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }

    /**
     * @OA\Post(
     *     path="/auth/login",
     *     tags={"auth"},
     *     summary="Login user",
     *     operationId="login",
     * @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="Email for authorization",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     * @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="Password for authorization",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),    
     * @OA\Response(
     *         response=200,
     *         description="Authorized status, generated token and token expiration date",
     *         @OA\JsonContent(
     *             type="object",
     * 
     *                 @OA\Property(property="access_token", type="integer", example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1"),
     *                 @OA\Property(property="token_type", type="string", example="Bearer"),
     *                 @OA\Property(property="expires_at", type="string", example="2019-05-06 09:34:52"),
     *         ),
     *     )
     * )
     */

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if ( ! $token = \JWTAuth::attempt($credentials)) {
            sleep(2);
            return response([
                'status' => 'error',
                'error' => 'invalid.credentials',
                'msg' => 'Invalid Credentials.'
            ], 400);
        }
        return response([
                'status' => 'success'
            ])
            ->header('Authorization', $token);
    }

    /**
     * @OA\Get(
     *     path="/auth/user",
     *     tags={"auth"},
     *     summary="Get logged in user info",
     *     operationId="user",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *         description="user data",
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);

        return response([
                'status' => 'success',
                'data' => [
                    'user' => $user,
                    'role' => $user->status(),
                    'clientFee' =>  $user->client ? $user->client->fee : null 
                ]
            ]);
    }

    /**
     * @OA\Get(
     *     path="/auth/refresh",
     *     tags={"auth"},
     *     summary="Refresh token",
     *     operationId="refresh",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *          description="refresh token",
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function refresh()
    {
        return response([
                'status' => 'success'
            ]);
    }

    /**
     * @OA\Post(
     *     path="/auth/logout",
     *     tags={"auth"},
     *     summary="Logout user",
     *     operationId="logout",
     * @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     ),   
     * @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization token",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ), 
     * @OA\Response(
     *         response=200,
     *          description="Logged out and revoked token",
     *     ),
     *  security={{"bearerAuth":{}}}
     * )
     */

    public function logout()
    {
        JWTAuth::invalidate();
        return response([
                'status' => 'success',
                'msg' => 'Logged out Successfully.'
            ], 200);
    }
}
