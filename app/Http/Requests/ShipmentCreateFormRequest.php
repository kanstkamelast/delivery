<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShipmentCreateFormRequest extends FormRequest
{
    public function authorize()
    {
         return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|unique:shipments',
            'type' => 'required|string',
            'delivery_time' => 'required|string',
            'comments' => 'required',
            'payment' => 'required',

        ];
    }
}
