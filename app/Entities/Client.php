<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';

    protected $fillable = [
        'name',
        'address',
        'state',
        'city',
        'country',
        'phone',
        'id_number',
        'user_percent',
        'fee',
    ];
}
