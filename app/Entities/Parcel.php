<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Carbon\Carbon;
use Auth;


class Parcel extends Model
{
    // Parcel Types
    const PT_REGULAR = 0,
    PT_LARGE = 1,
    // Regular Parcel Type values and coefs
    RPT_MIN = 25,
    RPT_MAX = 100,
    RPT_INSURANCE_FEE = 0.9,
    // Large Package Type values and coefs
    LPT_PER_M3 = 210,
    LPT_PER_POUND = 1.5,
    LPT_FEE = 46,
    LPT_INSURANCE_FEE = 0.9;
    
    protected $table = 'parcels';

    protected $fillable = [
        'country_id',
        'shipment_id',
        'sender_id',
        'client_id',
        'user_id',
        'recipient_id',
        'weight',
        'height',
        'length',
        'payment',
        'total_fee',
        'width',
        'products',
        'status_name',
        'status_comment',
        'type',
        'insurance',
    ];

    public function shipment()
    {
        return $this->hasOne(Shipment::class, 'id', 'shipment_id');
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function sender()
    {
        return $this->hasOne(Customer::class, 'id', 'sender_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function client()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    public function recipient()
    {
        return $this->hasOne(Customer::class, 'id', 'recipient_id');
    }

    public function currentParcels()
    {
        if(Auth::user()->status() == 'Client Admin') {
            return $this->where('client_id', Auth::user()->client_id)
                ->where('status_name', null)
                ->count();
        }
        if(Auth::user()->status() == 'User') {
            return $this->where('client_id', Auth::user()->client_id)
                ->where('user_id', Auth::user()->id)
                ->where('status_name', null)
                ->count();
        }
        return $this->where('status_name', null)
            ->count();
    }

    public function sentParcelsToday()
    {
        if(Auth::user()->status() == 'Client Admin') {
            return $this->where('client_id', Auth::user()->client_id)
                ->where('status_name', 'Shipped')
                ->whereDate('updated_at', Carbon::today())
                ->count();
        }
        if(Auth::user()->status() == 'User') {
            return $this->where('client_id', Auth::user()->client_id)
                ->where('user_id', Auth::user()->id)
                ->where('status_name', 'Shipped')
                ->whereDate('updated_at', Carbon::today())
                ->count();
        }
        
        return $this->where('status_name', 'Shipped')
            ->whereDate('updated_at', Carbon::today())
            ->count();
    }

    public function receivedParcelsToday()
    {
        if(Auth::user()->status() == 'Client Admin') {
            return $this->where('client_id', Auth::user()->client_id)
                ->where('status_name', 'Received')
                ->whereDate('updated_at', Carbon::today())
                ->count();
        }
        if(Auth::user()->status() == 'User') {
            return $this->where('client_id', Auth::user()->client_id)
                ->where('user_id', Auth::user()->id)
                ->where('status_name', 'Received')
                ->whereDate('updated_at', Carbon::today())
                ->count();
        }
        
        return $this->where('status_name', 'Received')->whereDate('updated_at', Carbon::today())->count();
    }

    public function monthGrouped()
    {
        $parcelsEntity = $this->select('id', 'created_at');

        if(Auth::user()->status() == 'Client Admin') $parcelsEntity->where('client_id', Auth::user()->client_id);
        if(Auth::user()->status() == 'User') $parcelsEntity->where('client_id', Auth::user()->client_id)->where('user_id', Auth::user()->id);

        $parcels = $parcelsEntity->get()
        ->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('m');
        });

        $parcelsCount = [];
        $parcelsArr = [];
        
        foreach ($parcels as $key => $value) {
            $parcelsCount[(int)$key] = count($value);
        }
        
        for($i = 1; $i <= 12; $i++){
            if(!empty($parcelsCount[$i])){
                $parcelsArr[] = $parcelsCount[$i];    
            }else{
                $parcelsArr[] = 0;    
            }
        }

        return $parcelsArr;
    }

    function randomColorPart()
    {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }
    
    function randomColor()
    {
        return '#'.$this->randomColorPart().$this->randomColorPart().$this->randomColorPart();
    }

    public function countryGrouped()
    {
        $parcelsEntity = $this->select('id', 'country_id');
        
        if(Auth::user()->status() == 'Client Admin') $parcelsEntity->where('client_id', Auth::user()->client_id);
        if(Auth::user()->status() == 'User') $parcelsEntity->where('client_id', Auth::user()->client_id)->where('user_id', Auth::user()->id);
        
        $parcels = $parcelsEntity->get()->groupBy('country_id');

        $countries = [];
        $values = [];
        $colors = [];

        foreach($parcels as $parcel) {
            $countries[] = $parcel->first()->country->name;
            $values[] = count($parcel);
            $colors[] = $this->randomColor();
        }

        return [$countries, $values, $colors];
    }


    public function allByStatus($filterData = null, $statusName = null)
    {
        $parcels = $this->where('status_name', $statusName)
            ->with('recipient', 'sender', 'shipment', 'country');
        
        if (!empty($filterData)) {
            if (array_key_exists('dateFrom', $filterData)) {
                is_null($filterData['dateFrom']) ?: $parcels->where('created_at', '>', $filterData['dateFrom']);
            }

            if (array_key_exists('dateTo', $filterData)) {
                is_null($filterData['dateTo']) ?: $parcels->where('created_at', '<', Carbon::createFromFormat('Y-m-d', $filterData['dateTo'])->addDay()->format('Y-m-d'));
            } 

            if (array_key_exists('shipmentId', $filterData)) {
                is_null($filterData['shipmentId']) ?: $parcels->where('shipment_id', $filterData['shipmentId']);
            }

            if (array_key_exists('reportId', $filterData)) {
                $field = $statusName == 'ShippedByAdmin'
                    ? 'admin_parcels_release_id' : 'parcels_release_id';
                is_null($filterData['reportId']) ?: $parcels->where($field, $filterData['reportId']);
            }

            if (array_key_exists('userId', $filterData)) {
                is_null($filterData['userId']) ?: $parcels->where('user_id', $filterData['userId']);
            }
        }

        if (Auth::user()->status() == 'Super Admin') {
            $parcels->with('user');
        }
        if (Auth::user()->status() == 'Client Admin') {
            $parcels->where('client_id', Auth::user()->client_id);
        }
        if (Auth::user()->status() == 'User') {
            $parcels->where('client_id', Auth::user()->client_id)
                ->where('user_id', Auth::user()->id);
        }

        return $parcels->orderBy('created_at', 'asc')
            ->get();
    }
}
