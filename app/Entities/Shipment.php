<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use DB;

class Shipment extends Model
{
    protected $table = 'shipments';

    protected $fillable = [
        'type',
        'name',
        'payment',
        'delivery_time',
        'comments',
    ];

    public static function getTypes()
    {
        return self::pluck('type')->toArray();
    }
}
