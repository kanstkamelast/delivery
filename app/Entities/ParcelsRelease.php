<?php

namespace App\Entities;


use App\User;
use Auth;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Model;

class ParcelsRelease extends Model
{
    const TABLE_NAME = 'parcels_release';

    protected $table = self::TABLE_NAME;

    protected $fillable = [];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public static function send($parcels, $status = null)
    {
        if (empty($parcels)) {
            return false;
        }
        $isAdminReport = !empty($status);
        $clientLastReportId = $isAdminReport
            ? self::where('user_id', Auth::user()->id)->max('id_per_client')
            : self::where('client_id', Auth::user()->client_id)->max('id_per_client');
        DB::beginTransaction();
        try {
            $model = new self;
            $model->id_per_client = empty($clientLastReportId) ? 1 : $clientLastReportId + 1;
            $model->client_id = Auth::user()->client_id;
            $model->user_id = Auth::user()->id;
            $model->save();

            $field = $isAdminReport ? 'admin_parcels_release_id' : 'parcels_release_id';
            $newStatus = $isAdminReport ? 'ShippedByAdmin' : 'Shipped';
            foreach ($parcels as $parcel) {
                $parcel->$field = $model->id;
                $parcel->status_name = $newStatus;
                $parcel->save();
            }
            DB::commit();
            return $model->id;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    public static function allReports($filterData = null)
    {
        $query = DB::table(self::TABLE_NAME)
            ->select(DB::raw('
                parcels_release.id,
                parcels_release.created_at,
                count(parcels.id) as parcel_count,
                sum(parcels.total_fee) as parcel_payment'))
            ->join('parcels', 'parcels_release.id', '=', 'parcels.parcels_release_id');

        $query->where('status_name', 'Shipped');

        if (!empty($filterData)) {
            if (array_key_exists('dateFrom', $filterData)) {
                is_null($filterData['dateFrom']) ?: $query->where('created_at', '>', $filterData['dateFrom']);
            }

            if (array_key_exists('dateTo', $filterData)) {
                is_null($filterData['dateTo']) ?: $query->where('created_at', '<', Carbon::createFromFormat('Y-m-d', $filterData['dateTo'])->addDay()->format('Y-m-d'));
            }

            if (array_key_exists('userId', $filterData)) {
                is_null($filterData['userId']) ?: $query->where('parcels_release.user_id', $filterData['userId']);
            }
        }

        if (Auth::user()->status() == 'Super Admin') {
            $query->addSelect(DB::raw('
                concat(
                    upper(substring(clients.name, 1, 3)),
                    lpad(clients.id, 4, "0"), "#",
                    lpad(parcels_release.id_per_client, 4, "0")
                ) as waybill, users.name as username'));
            $query->join('users', 'users.id', '=', 'parcels_release.user_id');
            $query->join('clients', 'clients.id', '=', 'parcels_release.client_id');
        } else {
            $clientName = empty(Auth::user()->client) ? '' : Auth::user()->client->name;
            $clientPrefix =  strtoupper(substr($clientName, 0, 3))
                . str_pad(Auth::user()->client_id, 3, '0', STR_PAD_LEFT) . '#';
            $query->addSelect(DB::raw('concat("' . $clientPrefix
                . '", lpad(parcels_release.id_per_client, 4, "0")) as waybill'));
        }
        if (Auth::user()->status() == 'Client Admin') {
            $query->where('parcels_release.client_id', Auth::user()->client_id);
            $query->where('parcels.client_id', Auth::user()->client_id);
        }
        if (Auth::user()->status() == 'User') {
            $query->where('parcels_release.client_id', Auth::user()->client_id)
                ->where('parcels_release.user_id', Auth::user()->id);
            $query->where('parcels.client_id', Auth::user()->client_id)
                ->where('parcels.user_id', Auth::user()->id);
        }
        $query->groupBy('parcels_release.id');

        return $query->get();
    }

    public static function allAdminReports($filterData = null)
    {
        $query = DB::table(self::TABLE_NAME)
            ->select(DB::raw('
                concat(
                    upper(substring(users.name, 1, 3)),
                    lpad(users.id, 4, "0"), "#",
                    lpad(parcels_release.id_per_client, 4, "0")
                ) as waybill,
                users.name as username,
                parcels_release.id,
                parcels_release.created_at,
                count(parcels.id) as parcel_count,
                sum(parcels.total_fee) as parcel_payment'))
            ->join('parcels', 'parcels_release.id', '=', 'parcels.admin_parcels_release_id');

        $query->where('status_name', 'ShippedByAdmin');

        if (!empty($filterData)) {
            if (array_key_exists('dateFrom', $filterData)) {
                is_null($filterData['dateFrom']) ?: $query->where('created_at', '>', $filterData['dateFrom']);
            }

            if (array_key_exists('dateTo', $filterData)) {
                is_null($filterData['dateTo']) ?: $query->where('created_at', '<', Carbon::createFromFormat('Y-m-d', $filterData['dateTo'])->addDay()->format('Y-m-d'));
            }
        }

        $query->join('users', 'users.id', '=', 'parcels_release.user_id');
        $query->groupBy('parcels_release.id');

        return $query->get();
    }
}
