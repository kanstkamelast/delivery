<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    const SUPER_ADMIN = 0;
    const CLIENT_ADMIN = 1;
    const USER = 2;

    /**
     * @var array
     */
    private $statuses = [];

    public function __construct()
    {
        $this->statuses = [
            self::SUPER_ADMIN => 'Super Admin',
            self::CLIENT_ADMIN => 'Client Admin',
            self::USER => 'User',
        ];
    }

    /**
     * Get the available statuses
     * @return array
     */
    public function lists()
    {
        return $this->statuses;
    }

    /**
     * Get the post status
     * @param int $statusId
     * @return string
     */
    public function get($statusId)
    {
        if (isset($this->statuses[$statusId])) {
            return $this->statuses[$statusId];
        }

        return $this->statuses[self::USER];
    }

    public function getIdByName($name)
    {
        return array_search($name, $this->statuses);
    }
}
