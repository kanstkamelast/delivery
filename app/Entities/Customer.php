<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    protected $fillable = [
        'last_name',
        'first_name',
        'company_name',
        'street',
        'building',
        'house_number',
        'zip_code',
        'city',
        'state',
        'country',
        'phone_number',
        'email',
    ];

    public function allCustomers()
    {
        return $this->all()->count();
    }
}
