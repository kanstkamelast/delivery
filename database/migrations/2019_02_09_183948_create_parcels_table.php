<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParcelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcels', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('country_id')->nullable()->unsigned();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            
            $table->integer('shipment_id')->nullable()->unsigned();
            $table->foreign('shipment_id')->references('id')->on('shipments')->onDelete('cascade');
            
            $table->integer('sender_id')->nullable()->unsigned();
            $table->foreign('sender_id')->references('id')->on('customers')->onDelete('cascade');
            
            $table->integer('recipient_id')->nullable()->unsigned();
            $table->foreign('recipient_id')->references('id')->on('customers')->onDelete('cascade');

            $table->string('weight');
            $table->string('height');
            $table->string('length');
            $table->string('payment');
            $table->string('total_fee');
            $table->string('width');
            $table->text('products');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcels');
    }
}
