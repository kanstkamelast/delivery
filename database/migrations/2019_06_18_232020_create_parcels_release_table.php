<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParcelsReleaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcels_release', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('id_per_client')->nullable()->unsigned();

            $table->integer('client_id')->nullable()->unsigned();
            $table->foreign('client_id', 'parcels_release_client_id_foreign')->references('id')->on('client')->onDelete('set null');

            $table->integer('user_id')->nullable()->unsigned();
            $table->foreign('user_id', 'parcels_release_user_id_foreign')->references('id')->on('users')->onDelete('set null');

            $table->timestamps();
        });

        Schema::table('parcels', function (Blueprint $table) {
            $table->integer('parcels_release_id')->nullable()->unsigned();
            $table->foreign('parcels_release_id', 'parcels_parcels_release_id_foreign')
                ->references('id')->on('parcels_release')->onDelete('set null');

            $table->integer('admin_parcels_release_id')->nullable()->unsigned();
            $table->foreign('admin_parcels_release_id', 'parcels_admin_parcels_release_id_foreign')
                ->references('id')->on('parcels_release')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parcels', function(Blueprint $table) {
            $table->dropForeign('parcels_parcels_release_id_foreign');
            $table->dropColumn('parcels_release_id');
        });
        Schema::table('parcels_release', function(Blueprint $table) {
            $table->dropForeign('parcels_release_client_id_foreign');
            $table->dropForeign('parcels_release_user_id_foreign');
        });
        Schema::dropIfExists('parcels_release');
    }
}
